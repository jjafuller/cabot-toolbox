import json
import psycopg2

from dynaconf import settings


def _get_connection():
    return psycopg2.connect(
        dbname=settings.get("PSQL_DATABASE"),
        user=settings.get("PSQL_USER"),
        password=settings.get("PSQL_PASS"),
        host=settings.get("PSQL_HOST"),
        port=settings.get("PSQL_PORT"),
        options="-c statement_timeout=0",
    )


def _get_id_for_user_by_username(conn, username):
    sql = "select id from auth_user where username = %(username)s"
    with conn.cursor() as cursor:
        cursor.execute(sql, {"username": username})
        return cursor.fetchone()


def _get_userprofile_by_user_id(conn, user_id):
    sql = """
        select id
        from cabotapp_userprofile
        where user_id = %(user_id)s
    """
    with conn.cursor() as cursor:
        cursor.execute(sql, {"user_id": user_id})
        return cursor.fetchone()


def _create_userprofile_for_user(conn, user_id):
    sql = """
        INSERT INTO public.cabotapp_userprofile
        (user_id, mobile_number, hipchat_alias, fallback_alert_user)
        VALUES (%(user_id)s, '', '', false)
    """
    with conn.cursor() as cursor:
        return cursor.execute(sql, {"user_id": user_id})


def _get_plugin_id_for_user_by_userprofile_id(conn, user_id):
    sql = """
        select id
        from cabotapp_alertpluginuserdata
        where user_id = %(user_id)s and title = 'Pagerduty Plugin'
    """
    with conn.cursor() as cursor:
        cursor.execute(sql, {"user_id": user_id})
        return cursor.fetchone()


def _create_plugin_for_user(conn, user_id):
    sql = """
        INSERT INTO public.cabotapp_alertpluginuserdata
        (polymorphic_ctype_id, title, user_id)
        VALUES (18, 'Pagerduty Plugin', %(user_id)s)
    """
    with conn.cursor() as cursor:
        return cursor.execute(sql, {"user_id": user_id})


def _get_service_key_for_user_by_plugin_id(conn, plugin_id):
    sql = """
        select service_key
        from cabot_alert_pagerduty_pagerdutyalertuserdata
        where alertpluginuserdata_ptr_id = %(plugin_id)s
    """
    with conn.cursor() as cursor:
        cursor.execute(sql, {"plugin_id": plugin_id})
        return cursor.fetchone()


def _create_service_key_for_user(conn, plugin_id, service_key):
    sql = """
        INSERT INTO public.cabot_alert_pagerduty_pagerdutyalertuserdata
        (alertpluginuserdata_ptr_id, service_key)
        VALUES (%(plugin_id)s, %(service_key)s)
    """
    with conn.cursor() as cursor:
        return cursor.execute(sql, {"plugin_id": plugin_id, "service_key": service_key})


def _update_service_key_for_user(conn, plugin_id, service_key):
    sql = """
        UPDATE public.cabot_alert_pagerduty_pagerdutyalertuserdata
        SET service_key = %(service_key)s
        WHERE alertpluginuserdata_ptr_id = %(plugin_id)s
    """
    with conn.cursor() as cursor:
        return cursor.execute(sql, {"plugin_id": plugin_id, "service_key": service_key})


def main():
    data = {}
    with open("pagerduty_integration_data.json") as json_file:
        data = json.load(json_file)

    users = [x for x in data["cabot_to_pagerduty_map"].keys()]

    with _get_connection() as conn:
        for user in users:
            print(user)
            user_id = _get_id_for_user_by_username(conn, user)
            if not user_id:
                raise Exception(f"User {user} was not found!")

            userprofile_id = _get_userprofile_by_user_id(conn, user_id)
            if not userprofile_id:
                _create_userprofile_for_user(conn, user_id)
                userprofile_id = _get_userprofile_by_user_id(conn, user_id)
                print(userprofile_id)

            plugin_id = _get_plugin_id_for_user_by_userprofile_id(conn, userprofile_id)
            if not plugin_id:
                _create_plugin_for_user(conn, userprofile_id)
                plugin_id = _get_plugin_id_for_user_by_userprofile_id(
                    conn, userprofile_id
                )
                print(plugin_id)

            service_key = _get_service_key_for_user_by_plugin_id(conn, plugin_id)
            print(service_key)
            if not service_key:
                pduser = data["cabot_to_pagerduty_map"][user]
                _create_service_key_for_user(
                    conn, plugin_id, data["integration_keys"][pduser]
                )
                service_key = _get_service_key_for_user_by_plugin_id(conn, plugin_id)
                print(service_key)
            elif not service_key[0]:
                pduser = data["cabot_to_pagerduty_map"][user]
                _update_service_key_for_user(
                    conn, plugin_id, data["integration_keys"][pduser]
                )
                service_key = _get_service_key_for_user_by_plugin_id(conn, plugin_id)
                print(service_key)


if __name__ == "__main__":
    main()
